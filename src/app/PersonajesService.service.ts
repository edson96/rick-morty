import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, Subscriber } from "rxjs";

import { listaPersonajes } from './models/listaPersonajes.interface';

@Injectable({
    providedIn: 'root'
})

export class PersonajesService{
    url:string = "https://rickandmortyapi.com/api/character/1,2,3,4,5,6";
    url_buscar:string =  "https://rickandmortyapi.com/api/character/?";

    constructor(private httpClient:HttpClient){}

    primerosPersonajes(){
        return this.httpClient.get<any>(this.url);
    }

    buscarPersonajes(q: string, f:string){
        let direccion = this.url_buscar+"name="+q+"&"+f;
        return this.httpClient.get<any>(direccion);
    }
}