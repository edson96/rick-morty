import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Subscriber, Observable } from 'rxjs';

import { PersonajesService } from '../PersonajesService.service';
import { Personaje } from '../models/personajes.model';
import { Filtro } from '../models/filtro.model';

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.css']
})
export class PersonajesComponent implements OnInit {

  respuesta: Personaje[] = [];
  busqueda: Filtro[]=[];
  url_svg = "/assets/svg/heart_inactive.svg";
  closeResult = '';

  // Datos por personaje
    name = "";
    species="";
    gender="";
    image="";
    status="";
    episode={};

    q = "";
    f = "status=alive";

  constructor(
    private PersonajesService:PersonajesService, 
    private modalService: NgbModal) { }


  ngOnInit(): void {
    this.PersonajesService.primerosPersonajes()
    .subscribe(
      (respuesta: Personaje[])=>{
        this.respuesta = respuesta; 
        console.log(respuesta[0]);
      }
    );
  }

  buscarPersonaje(event:Event){
    var q = (<HTMLInputElement>event.target).value;
    this.q = q;
    this.PersonajesService.buscarPersonajes(this.q, this.f)
    .subscribe(
      (respuesta: Filtro)=>{ 
        this.respuesta = respuesta['results'];
        console.log(respuesta['results']);
      }
    );
  }

  buscarPersonajesPor(event:Event){
    var f = (<HTMLInputElement>event.target).value;
    this.f = f;
    this.PersonajesService.buscarPersonajes(this.q, this.f)
    .subscribe(
      (respuesta: Filtro)=>{ 
        this.respuesta = respuesta['results'];
        console.log(respuesta['results']);
      }
    );
  }

  swithHeart(image:any){
    if(image.activo){
      image.src = "/assets/svg/heart_inactive.svg";
      image.activo = false;
    }else {
      image.src = "/assets/svg/heart_active.svg";
      image.activo = true;
    }
  }

  open(content:any, personaje:Personaje) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

    this.name = personaje.name;
    this.species = personaje.species;
    this.gender = personaje.gender;
    this.status = personaje.status;
    this.episode = personaje.episode;
    this.image = personaje.image;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
