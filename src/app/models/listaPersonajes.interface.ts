export class listaPersonajes{
    constructor(
    public id:number,
    public name:string,
    public species:string,
    public gender:string,
    public image:string,
    public status:string,
    public episode:string[]){}
}