export interface buscarPersonajes{
    results:[{
        id:number,
        name:string,
        species:string,
        gender:string,
        image:string,
        status:string,
        episode:string[]
    }];
}